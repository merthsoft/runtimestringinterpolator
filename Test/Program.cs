﻿using RuntimeStringInterpolator;
using System;
using System.Collections.Generic;

namespace Test {
    class Program {
        static void Main(string[] args) {
            var values = new Dictionary<string, object> {
                { "label", "Basic Padded" },
                { "thing_name", "Rifle" },
                { "version", 30 }
            };

            Console.WriteLine("{label} ({{label}}) - {thing_name} ({{thing_name}}) v{version:X2} ({{version:X2}})".Interpolate(values));
            Console.ReadLine();
        }
    }
}
