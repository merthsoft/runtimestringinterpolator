﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RuntimeStringInterpolator {
    public static class RuntimeStringInterpolator {
        public const char INTERPOLATION_START = '{';
        public const char INTERPOLATION_END = '}';
        public const char FORMAT_SEPARATOR = ':';

        private enum State { BuildingString, BuildingInterpolation, BuildingFormat, BuildingString_EscapeEnd }


        [Serializable]
        public class InterpolationException : Exception {
            public InterpolationException() { }
            public InterpolationException(string message) : base(message) { }
            public InterpolationException(string message, Exception inner) : base(message, inner) { }
            protected InterpolationException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
        }

        public static string Interpolate(this string s, Dictionary<string, object> args) {
            var builder = new StringBuilder(s.Length + args.Count() * 8);

            var formatBuffer = new StringBuilder();
            var interpolationBuffer = new StringBuilder();

            State state = State.BuildingString;
            foreach (var c in s) {
                switch (state) {
                    case State.BuildingString:
                        switch (c) {
                            case INTERPOLATION_START:
                                state = State.BuildingInterpolation;
                                break;
                            case INTERPOLATION_END:
                                state = State.BuildingString_EscapeEnd;
                                break;
                            default:
                                builder.Append(c);
                                break;
                        }
                        break;
                    case State.BuildingString_EscapeEnd:
                        switch (c) {
                            case INTERPOLATION_END:
                                builder.Append(c);
                                state = State.BuildingString;
                                break;
                            default:
                                throw new InterpolationException($"'{INTERPOLATION_END}' must be escaped by another '{INTERPOLATION_END}' character.");
                        }
                        break;
                    case State.BuildingInterpolation:
                        switch (c) {
                            case FORMAT_SEPARATOR:
                                state = State.BuildingFormat;
                                break;
                            case INTERPOLATION_START:
                                builder.Append(c);
                                state = State.BuildingString;
                                break;
                            case INTERPOLATION_END:
                                var key = interpolationBuffer.ToString();
                                if (!args.ContainsKey(key)) {
                                    throw new InterpolationException($"Unable to find key {key} in args.");
                                }
                                var obj = args[key];
                                builder.Append(obj);
                                interpolationBuffer.Length = 0;
                                state = State.BuildingString;
                                break;
                            default:
                                interpolationBuffer.Append(c);
                                break;
                        }
                        break;
                    case State.BuildingFormat:
                        switch (c) {
                            case INTERPOLATION_END:
                                var key = interpolationBuffer.ToString();
                                if (!args.ContainsKey(key)) {
                                    throw new InterpolationException($"Unable to find key {key} in args.");
                                }
                                var obj = args[key];
                                var formattable = obj as IFormattable;
                                builder.Append(formattable?.ToString(formatBuffer.ToString(), null) ?? obj);
                                interpolationBuffer.Length = 0;
                                formatBuffer.Length = 0;
                                state = State.BuildingString;
                                break;
                            default:
                                formatBuffer.Append(c);
                                break;
                        }
                        break;
                }
            }

            return builder.ToString();
        }
    }
}
